package tips;

import java.net.URL;
import java.util.ResourceBundle;

import arithmometr.animation.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    private TextField totalInRuble;

    @FXML
    private Button buttonClear;

    @FXML
    private Button buttonDoNotTip;

    @FXML
    private Button button5;

    @FXML
    private Button button10;

    @FXML
    private TextField bill;

    @FXML
    private TextField billInRuble;

    @FXML
    private TextField tipCurrency;

    @FXML
    private TextField tipRubles;

    @FXML
    private TextField total;

    @FXML
    private Button buttonCalculate;

    double input() {
        return Double.parseDouble(bill.getText());
    }

    @FXML
    void calculate(ActionEvent event) {
        if (billInRuble.getText().isEmpty() && tipCurrency.getText().isEmpty() && tipRubles.getText().isEmpty()) {
            Shake rubAnim = new Shake(billInRuble);
            Shake tipCurAnim = new Shake(tipCurrency);
            Shake tipRubAnim = new Shake(tipRubles);
            rubAnim.playAnimation();
            tipCurAnim.playAnimation();
            tipRubAnim.playAnimation();
            return;
        }

        double tipResult = Double.parseDouble(bill.getText()) + Double.parseDouble(tipCurrency.getText());
        double rubResult = Double.parseDouble(billInRuble.getText()) + Double.parseDouble(tipRubles.getText());
        total.setText(String.valueOf(tipResult));
        totalInRuble.setText(String.valueOf(rubResult));
    }

    @FXML
    void clear(ActionEvent event) {
        bill.clear();
        billInRuble.clear();
        tipRubles.clear();
        tipCurrency.clear();
        totalInRuble.clear();
        total.clear();
    }

    @FXML
    void leave10(ActionEvent event) {
        String box1 = comboBox.getValue();

        if (bill.getText().isEmpty()) {
            Shake tipAnim = new Shake(bill);
            tipAnim.playAnimation();
            return;
        }

        if (box1.equals("Euro")) {
            double euro = input();
            if (euro < 0) {
                billInRuble.setText("Некорректный ввод!");
            } else {
                double rub = euro * 80;
                double tipsInCurrency = euro / 100 * 10;
                double tipsInRuble = rub / 100 * 10;
                billInRuble.setText(String.valueOf(rub));
                tipCurrency.setText(String.valueOf(tipsInCurrency));
                tipRubles.setText(String.valueOf(tipsInRuble));
            }
        }
        if (box1.equals("Dollar")) {
            double dollar = input();
            if (dollar < 0) {
                billInRuble.setText("Incorrect input!");
            } else {
                double rub = dollar * 72;
                double tipsInCurrency = dollar / 100 * 10;
                double tipsInRuble = rub / 100 * 10;
                billInRuble.setText(String.valueOf(rub));
                tipCurrency.setText(String.valueOf(tipsInCurrency));
                tipRubles.setText(String.valueOf(tipsInRuble));
            }
        }
    }

    @FXML
    void leave5(ActionEvent event) {
        String box1 = comboBox.getValue();

        if (bill.getText().isEmpty()) {
            Shake tipAnim = new Shake(bill);
            tipAnim.playAnimation();
            return;
        }

        if (box1.equals("Euro")) {
            double euro = input();
            if (euro < 0) {
                billInRuble.setText("Incorrect input!");
            } else {
                double rub = euro * 80;
                double tipsInCurrency = euro / 100 * 5;
                double tipsInRuble = rub / 100 * 5;
                billInRuble.setText(String.valueOf(rub));
                tipCurrency.setText(String.valueOf(tipsInCurrency));
                tipRubles.setText(String.valueOf(tipsInRuble));
            }
        }
        if (box1.equals("Dollar")) {
            double dollar = input();
            if (dollar < 0) {
                billInRuble.setText("Incorrect input!");
            } else {
                double rub = dollar * 72;
                double tipsInCurrency = dollar / 100 * 5;
                double tipsInRuble = rub / 100 * 5;
                billInRuble.setText(String.valueOf(rub));
                tipCurrency.setText(String.valueOf(tipsInCurrency));
                tipRubles.setText(String.valueOf(tipsInRuble));
            }
        }
    }

    @FXML
    void notLeave(ActionEvent event) {
        String box1 = comboBox.getValue();

        if (bill.getText().isEmpty()) {
            Shake tipAnim = new Shake(bill);
            tipAnim.playAnimation();
            return;
        }

        if (box1.equals("Euro")) {
            double euro = input();
            if (euro < 0) {
                billInRuble.setText("Incorrect input!");
            } else {
                double rub = euro * 80;
                billInRuble.setText(String.valueOf(rub));
                tipCurrency.setText(String.valueOf(0));
                tipRubles.setText(String.valueOf(0));
            }
        }
        if (box1.equals("Dollar")) {
            double dollar = input();
            if (dollar < 0) {
                billInRuble.setText("Incorrect input!");
            } else {
                double rub = dollar * 72;
                billInRuble.setText(String.valueOf(rub));
                tipCurrency.setText(String.valueOf(0));
                tipRubles.setText(String.valueOf(0));
            }
        }
    }

    @FXML
    void round(ActionEvent event) {
        if (total.getText().isEmpty() && totalInRuble.getText().isEmpty()) {
            Shake chequeAnim = new Shake(total);
            Shake chequeRubAnim = new Shake(totalInRuble);
            chequeAnim.playAnimation();
            chequeRubAnim.playAnimation();
            return;
        }
        total.setText(String.valueOf(Math.round(Float.parseFloat(String.valueOf(Double.parseDouble(total.getText()))))));
        totalInRuble.setText(String.valueOf(Math.round(Float.parseFloat(String.valueOf(Double.parseDouble(totalInRuble.getText()))))));
    }


    @FXML
    void initialize() {

    }
}


