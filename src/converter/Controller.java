package converter;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    public Label lengthWarning;

    @FXML
    private TextField numberLengthField;

    @FXML
    private TextField resultLengthField;

    @FXML
    private ComboBox<String> unitLengthComboBox1;

    @FXML
    private ComboBox<String> unitLengthComboBox2;

    @FXML
    void clear() {
        numberLengthField.clear();
        resultLengthField.clear();
    }

    @FXML
    void calculate() {
        if (unitLengthComboBox1.getSelectionModel().getSelectedIndex() == -1 || unitLengthComboBox2.getSelectionModel().getSelectedIndex() == -1) {
            lengthWarning.setText("Select unit!");
            lengthWarning.setVisible(true);
            return;
        } else lengthWarning.setVisible(false);
        if (numberLengthField.getText().equals("")) {
            lengthWarning.setText("You did not enter a number!");
            lengthWarning.setVisible(true);
            return;
        }

        lengthWarning.setVisible(false);

        switch (unitLengthComboBox1.getSelectionModel().getSelectedIndex()) {
            case 0:
                switch (unitLengthComboBox2.getSelectionModel().getSelectedIndex()) {
                    case 0:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 1));
                        break;
                    case 1:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) / 10));
                        break;
                    case 2:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) / 1000));
                        break;
                    case 3:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) / 1e+6));
                        break;
                }
                break;

            case 1:
                switch (unitLengthComboBox2.getSelectionModel().getSelectedIndex()) {
                    case 0:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 10));
                        break;
                    case 1:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 1));
                        break;
                    case 2:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) / 100));
                        break;
                    case 3:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) / 100000));
                        break;
                }
                break;

            case 2:
                switch (unitLengthComboBox2.getSelectionModel().getSelectedIndex()) {
                    case 0:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 1000));
                        break;
                    case 1:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 100));
                        break;
                    case 2:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 1));
                        break;
                    case 3:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) / 1000));
                        break;
                }
                break;

            case 3:
                switch (unitLengthComboBox2.getSelectionModel().getSelectedIndex()) {
                    case 0:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 1e+6));
                        break;
                    case 1:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 100000));
                        break;
                    case 2:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 1000));
                        break;
                    case 3:
                        resultLengthField.setText(String.valueOf(Double.parseDouble(numberLengthField.getText()) * 1));
                        break;
                }
                break;
        }
    }
}